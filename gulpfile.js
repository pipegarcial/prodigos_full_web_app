'use strict';
// ----------------------------------
var gulp = require('gulp');
var gulpCopy = require('gulp-copy');
var del = require('del');
// ----------------------------------
gulp.task('create-content-public', function () {
  gulp
    .src('./angular_app/dist/**/*.*')
    .pipe(gulp.dest('./public/'));
});

/*gulp.task('delete-index-dist', function () {
  return del([
    'public/index.html',
  ]);
});*/
// ----------------------------------
gulp.task('default',['create-content-public']);