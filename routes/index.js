var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
const nodemailer = require('nodemailer');
const pug = require('pug');


router.post('/email-new-service', function (req, res, next) {
    //settingParams
    let emailUser = req.body.whoAskService.email;
    let jsonInfoService = {
        'nameUser': req.body.whoAskService.name,
        'celUser': req.body.whoAskService.cel,
        'emailUser': emailUser,
        'forWhorService': req.body.forWhorService === true ? 'Para mi' : 'Para otra persona',
        'name': req.body.nameBeneficiary,
        'address': req.body.place.address,
        'helpAddress': req.body.place.helpAddress !== undefined ? req.body.helpAddress : '',
        'city': req.body.place.city,
        'sector': req.body.place.sector,
        'description': req.body.description,
        'recomendation': req.body.recomendation,
        'datePicked': req.body.date.datePicked.split('/').reverse().join('/'),
        'timePicked': req.body.date.timePicked,
        'hoursService': req.body.cost.hours,
        'costHour': '$' + req.body.cost.perHour,
        'costTotal': '$' + req.body.cost.total,
        'titleEmail': req.body.infoEmail.titleEmail,
        'messageEmail': req.body.infoEmail.messageEmail
    };
    //Compiling and rendering pug render
    var compiledTemplate = pug.renderFile('./views/emails/new-service.pug', jsonInfoService);

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        service: 'GMAIL',
        port: 465,
        auth: {
            user: 'felipegarcial64@gmail.com',
            pass: 'deporcali2015'
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Pródigos" <fgarcia@prodigosapp.com>', // sender address
        to: emailUser + ',contacto@prodigosapp.com', // list of receivers
        subject: 'Nuevo servicio', // Subject line
        //text: 'Cordial saludo, hemos recibido tu solicitud a continuación econtrarás el resumen del servicio:', // plain text body
        html: compiledTemplate // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
        res.send();
    });
});

module.exports = router;